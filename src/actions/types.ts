export interface SongMessage {
  title: string
  artist: string
  duration: string
  image_url: string
}

export const SELECT_SONG = "SELECT_SONG"

export interface SongMessageAction {
  type: typeof SELECT_SONG
  payload: SongMessage
}
