import { SELECT_SONG, SongMessage } from "actions/types"

export const selectSong = (song: SongMessage) => {
  return {
    type: SELECT_SONG,
    payload: song,
  }
}
