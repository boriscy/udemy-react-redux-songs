import React from "react"
import { connect } from "react-redux"
import { SongMessage } from "actions/types"

interface SongDetailProps {
  song: SongMessage | null
}

const SongDetail: React.SFC<SongDetailProps> = ({ song }) => {
  if (!song) {
    return <div>Select a song</div>
  }
  return (
    <div className="song-detail">
      <img alt={song.title} src={song.image_url} />
      <div>
        <span title="Artist" className="artist">
          {song.artist}
        </span>
        {" - "}
        <span title="Song title" className="title">
          {song.title}
        </span>
        <span className="duration">{song.duration}</span>
      </div>
    </div>
  )
}

interface SongDetailState {
  selectedSong: SongMessage | null
}

const mapStateToProps = (state: SongDetailState = { selectedSong: null }) => {
  return { song: state.selectedSong }
}

export default connect(mapStateToProps)(SongDetail)
