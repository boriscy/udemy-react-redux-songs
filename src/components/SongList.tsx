import React, { Component } from "react"
import { connect } from "react-redux"
import { SongMessage } from "actions/types"
import { selectSong } from "actions"

interface SongListProps {
  songs: SongMessage[]
  selectSong: typeof selectSong
}

class SongList extends Component<SongListProps> {
  renderItem(song: SongMessage) {
    return (
      <div className="item" key={song.title}>
        <div className="left floated content">
          <img src={song.image_url} width="100" />
        </div>
        <div className="right floated content">
          <button
            className="ui button primary"
            onClick={() => this.props.selectSong(song)}
          >
            Select
          </button>
        </div>
        <div className="content">
          <h3 className="title">{song.title}</h3>
        </div>
      </div>
    )
  }

  render() {
    const { songs } = this.props
    return (
      <div className="song-list ui divided list">
        {songs.map((song) => this.renderItem(song))}
      </div>
    )
  }
}

const mapStateToProps = (state: SongListProps) => {
  return { songs: state.songs }
}

export default connect(mapStateToProps, { selectSong })(SongList)
