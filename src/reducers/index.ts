import { combineReducers } from "redux"
import { SELECT_SONG, SongMessageAction } from "actions/types"

export const songsReducer = (state = songs) => {
  return state
}

const songs = [
  {
    title: "Billie Jean",
    artist: "Michael Jackson",
    duration: "4:00",
    image_url: "https://duckduckgo.com/i/6273d0b0.png",
  },
  {
    title: "Frozen",
    artist: "Madona",
    duration: "5:23",
    image_url: "https://duckduckgo.com/i/0910ba41.png",
  },
  {
    title: "Under the Bridge",
    artist: "Red Hot Chile Peppers",
    image_url: "https://duckduckgo.com/i/23eab49b.jpg",
  },
  {
    title: "Take on Me",
    artist: "A-ha",
    image_url: "https://duckduckgo.com/i/af8f48ab.jpg",
  },
]

export const selectedSongReducer = (
  state = null,
  action: SongMessageAction
) => {
  if (action && action.type === SELECT_SONG) {
    return action.payload
  }
  return null
}

export default combineReducers({
  songs: songsReducer,
  selectedSong: selectedSongReducer,
})
